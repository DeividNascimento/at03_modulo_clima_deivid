## At03_modulo_clima_Deivid

Este repositório contém todas as pastas relacionadas a atividade 03 da disciplina Linguagem de Programação 2.
Para executar a módulo, é necessário seguir os passos abaixo.

####Executar os testes de unidade do módulo clima
1. Os testes de cada módulo estão no diretório clima/controllers/test
2. Para executar o teste de regressão do módulo, acesse **http://localhost/modulo/clima/test/all**

####Executar o código do módulo clima
1. Clone este repositório em um diretório chamado **At03_modulo_clima_Deivid**
2. Importe para o MySQL o arquivo tarefa_turma.sql que se encontra na pasta sql
3. Digite no navegador **http://localhost/modulo/clima**

####Funcionamento do módulo clima
1. Através do banco de dados é possivel guardar diversas perguntas para serem executadas pelo módulo
2. As respostas são armazenadas em um banco para serem analisadas posteriormente
3. Existe apenas a função excluir caso tenha alguma ficha de pesquisa com respostas não confiaveis.
4. Existe duas páginas de visualização, página de pesquisa (formulário) e página de resultados(lista de resustados).

---
