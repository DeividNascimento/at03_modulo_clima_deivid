<div class='float-left btn-group'>
    <a class="btn btn-info px-3" role="button" href="<?= base_url('Clima/index')?>">Ficha de Perguntas</a>
    <a class="btn btn-info px-3" role="button" href="<?= base_url('Clima/listarrespostas')?>">Resultados</a>

</div>
</div><br /><br />

<div class="btn-info text-center  mt-5">
    <h3>Resultados da pesquisa organizacional</h3>
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col" class="text-center">Identificação da ficha de pesquisa</th>
            <th scope="col" class="text-center">Resposta 1</th>
            <th scope="col" class="text-center">Resposta 2</th>
            <th scope="col" class="text-center">Resposta 3</th>
            <th scope="col" class="text-center">Resposta 4</th>
            <th scope="col" class="text-center">Resposta 5</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($clima_respostas as $resposta): ?>


        <tr>
            <td class="text-center"><?= $resposta->id ?></td>
            <td class="text-center"><?= $resposta->resposta1 ?></td>
            <td class="text-center"><?= $resposta->resposta2 ?></td>
            <td class="text-center"><?= $resposta->resposta3 ?></td>
            <td class="text-center"><?= $resposta->resposta4 ?></td>
            <td class="text-center"><?= $resposta->resposta5 ?></td>          
            
            
            <td class="text-center"><a class="btn btn-info px-3" role="button"
                    href="<?= base_url('Clima/excluirficha/'.$resposta->id)?>">Excluir</a></td>


        </tr>


        <?php  endforeach;  ?>


    </tbody>
</table>