
<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark default-color">
  <a class="navbar-brand" href="<?= base_url('Viajar')?>">ViagemVIVA.com</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Viajar/quemsomos')?>">Quem somos
          
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Viajar/compra')?>">Ofertas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Viajar/contato')?>">Contato</a>
      </li>
      
    </ul>
    <ul class="navbar-nav float-right mr-4 nav-flex-icons">
    <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Login 
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item" href="#">Cliente</a>
          <a class="dropdown-item" href="#">Empresas</a>
          <a class="dropdown-item" href="<?= base_url('Viajar/admincadastrar')?>">Administrador</a>
        </div>
           
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->