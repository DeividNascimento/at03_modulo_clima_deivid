<div class='float-left btn-group'>
    <a class="btn btn-info px-3" role="button" href="<?= base_url('Clima/index')?>">Ficha de Perguntas</a>
    <a class="btn btn-info px-3" role="button" href="<?= base_url('Clima/listarrespostas')?>">Resultados</a>

</div>
</div><br /><br />

<div class="btn-info text-center  mt-5">
    <h3>Responda a Pesquisa Organizacional</h3>
</div>

<form method = "POST" class="text border border-light p-5">


<?php foreach ($clima_questoes as $questao): ?>
					
						
<p class="h4 mb-4"><?= $questao->id ?>.<?= $questao->pergunta ?></p>    
    <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" id="<?= $questao->id ?>S" name="resposta<?= $questao->id ?>" value="Sim">
        <label class="custom-control-label" for="<?= $questao->id ?>S">Sim</label>
    </div>    
    <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" id="<?= $questao->id ?>A" name="resposta<?= $questao->id ?>" value="Às vezes" checked>
        <label class="custom-control-label" for="<?= $questao->id ?>A">Às vezes</label>
    </div>    
    <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" id="<?= $questao->id ?>N" name="resposta<?= $questao->id ?>" value="Não">
        <label class="custom-control-label" for="<?= $questao->id ?>N">Não</label>
    </div>
                    

    <?php  endforeach;  ?> 
 
    <button class="btn btn-info btn-block my-4" type="submit">Finalizar</button>

</form>
