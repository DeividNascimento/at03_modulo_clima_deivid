<?php 
    defined('BASEPATH') OR exit('No direct script acasse allowed');

    class ClimaModel extends CI_Model{

        

        /**
         * Pega as informações das questões para listar.
         * 
         * */
        public function get_dataquestoes(){
            $sql = "SELECT * FROM clima_questoes" ;
            $res = $this->db->query($sql);
            $data = $res->result();
            return $data;

        }
        /**
         * Pega as informações das respostas para listar.
         * 
         * */
        public function get_datarespostas(){
            $sql = "SELECT * FROM clima_respostas" ;
            $res = $this->db->query($sql);
            $data = $res->result();
            return $data;

        }
        /**
         * Cria uma nova ficha apartir do formulário.
         * 
         * */
        public function nova_ficha(){
            if(sizeof($_POST)==0) return;
            $this->load->library('Validator', null, 'valida');

            if($this->valida->clima_form()){
                $this->load->library('FichaClima', null, 'ficha');

            $data['resposta1'] = $this->input->post("resposta1");
            $data['resposta2'] = $this->input->post("resposta2");
            $data['resposta3'] = $this->input->post("resposta3");
            $data['resposta4'] = $this->input->post("resposta4");
            $data['resposta5'] = $this->input->post("resposta5");
            
            $this->db->insert('clima_respostas',$data);
            }
            else return true;

        }

        /**
         * Elimina uma ficha do bd.
         * @param int id: o identificador da ficha
         * */
        public function excluir_ficha($id){
            

            $this->db->where("id",$id);
            $this->db->delete("clima_respostas");

        }
      
        /**
         * Pega os dados de apenas uma ficha.
         * @param int id: o identificador da ficha
         * */   

        public function get_detalhe($id){
            $sql = "SELECT * FROM clima_respostas WHERE id = $id";
            $res = $this->db->query($sql);
            $data = $res->result();
            return $data;

        }
    
}