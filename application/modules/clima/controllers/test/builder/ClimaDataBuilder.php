<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class ClimaDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_climatest'){
        parent::__construct('lp2_modulo_climatest', $table);
    }

    function getData($index = -1){
        
        $data[0]['resposta1'] = 'Sim';
        $data[0]['resposta2'] = 'Sim';
        $data[0]['resposta3'] = 'Não';
        $data[0]['resposta4'] = 'Sim';
        $data[0]['resposta5'] = 'Ás vezes';

        $data[1]['resposta1'] = 'Não';
        $data[1]['resposta2'] = 'Sim';
        $data[1]['resposta3'] = 'Não';
        $data[1]['resposta4'] = 'Ás vezes';
        $data[1]['resposta5'] = 'Ás vezes';

        $data[2]['resposta1'] = 'Não';
        $data[2]['resposta2'] = 'Sim';
        $data[2]['resposta3'] = 'Sim';
        $data[2]['resposta4'] = 'Ás vezes';
        $data[2]['resposta5'] = 'Ás vezes';

        $data[3]['resposta1'] = 'Ás vezes';
        $data[3]['resposta2'] = 'Sim';
        $data[3]['resposta3'] = 'Ás vezes';
        $data[3]['resposta4'] = 'Não';
        $data[3]['resposta5'] = 'Não';
        
        return $index > -1 ? $data[$index] : $data;
    }

}