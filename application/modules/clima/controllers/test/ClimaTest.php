<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/clima/libraries/FichaClima.php';
include_once APPPATH . 'modules/clima/controllers/test/builder/ClimaDataBuilder.php';

class ClimaTest extends Toast{
    private $builder;
    private $ficha;

    function __construct(){
        parent::__construct('ClimaTest');
    }

    function _pre(){
        $this->builder = new ClimaDataBuilder();
        $this->ficha = new FichaClima();
    }

    // Verificação se os objetos foram criados corretamente
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->ficha, "Erro na criação da ficha");
    }

    // Verificação se o banco foi selecionado  corretamente
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // possibilidade de vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->ficha->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação se o objeto é o mesmo que foi enviado
        $task = $this->ficha->get(array('id' => 1))[0];
        
        $this->_assert_equals($data['resposta1'], $task['resposta1']);
        $this->_assert_equals($data['resposta2'], $task['resposta2']);
        $this->_assert_equals($data['resposta3'], $task['resposta3']);
        $this->_assert_equals($data['resposta4'], $task['resposta4']);
        $this->_assert_equals($data['resposta5'], $task['resposta5']);

        // possibilidade de vetor vazio
        $id2 = $this->ficha->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // possibilidade de vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->ficha->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // possibilidade de vetor com dados incompletos... deve ser
        
        $v = array('resposta1' => 'vetor incompleto');
        $id = $this->ficha->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }
         // Teste para verificar se todos os resgistros foram para tabela
    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->ficha->get();
        $this->_assert_equals(4, sizeof($tasks), "Número de registros incorreto");
    }
        // Teste para carregar todos os registros
    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->ficha->get(array('resposta3' => 'Não', 'resposta5' => 'Ás vezes'))[0];
        $this->_assert_equals('Não', $task['resposta3'], "Erro ao carregar o registro  na resposta3");
        $this->_assert_equals('Ás vezes', $task['resposta5'], "Erro ao carregar o registro na resposta5");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // Leitura do banco de dados
        $task1 = $this->ficha->get(array('id' => 3))[0];
        $this->_assert_equals('Sim', $task1['resposta3'], "Erro de leitura na resposta3");
        $this->_assert_equals('Ás vezes', $task1['resposta5'], "Erro de leitura na resposta5");

        // Atualizando valores
        $task1['resposta3'] = 'Ás vezes';
        $task1['resposta5'] = 'Sim';
        $this->ficha->insert_or_update($task1);

        // leitura novamente do mesmo objeto para confirmar a atualização correta
        $task2 = $this->ficha->get(array('id' => 3))[0];
        $this->_assert_equals($task1['resposta3'], $task2['resposta3'], "Erro na atualização resposta3");
        $this->_assert_equals($task1['resposta5'], $task2['resposta5'], "Erro na atualização resposta5");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // Verificando se realmente o registro existe
        $task1 = $this->ficha->get(array('id' => 1))[0];
        $this->_assert_equals('Não', $task1['resposta3'], "Erro de verificação na resposta3");
        $this->_assert_equals('Ás vezes', $task1['resposta5'], "Erro de verificação na resposta5");

        // Apagando o registro da tabela
        $this->ficha->delete(array('id' => 1));

        // Verifica se realmente apagou o registro
        $task2 = $this->ficha->get(array('id' => 1));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}