<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Clima extends MY_Controller{

    //public function __construct(){
    //    $this->load->model('ClimaModel', 'model');
    //}

    /**
     * Página para exibir as perguntas da pesquisa a ser realizada
     **/
    public function index()
    {
        $this->load->view('common/header');
        $this->load->model('ClimaModel');
        $this->ClimaModel->nova_ficha();
        $data['clima_questoes'] = $this->ClimaModel->get_dataquestoes();
        
        $this->load->view('clima/clima_form',$data);
        
        $this->load->view('common/footer');
       
    }
    /**
     * Página para listar as fichas com sua respectivas respostas
     **/
    public function listarrespostas()
    {
        $this->load->view('common/header');
        $this->load->model('ClimaModel');

        $data['clima_respostas'] = $this->ClimaModel->get_datarespostas();
        $this->load->view('clima/clima_list',$data);
        $this->load->view('common/footer');
       
    }

    /**
     * Página excluir alguma ficha inconsistente ou inválida
     * @param int id: o id da ficha a ser excluida
     */
    public function excluirficha($id)
    {
        $this->load->view('common/header');
        $this->load->model('ClimaModel');

        $this->ClimaModel->excluir_ficha($id);

        $this->load->view('clima/excluir_ficha');
        $this->load->view('common/footer');
    }



    /**
     * Página para criação de tarefas
     * @param int turma_id: o id da turma para a qual será direcionada a tarefa
     */
    public function criar($turma_id){
        $this->add_script('tarefa/mascara');
        
        $this->validate_id($turma_id);
        $turma = $this->model->nome_turma($turma_id);
        $data['show_form'] = $this->model->nova_tarefa($turma_id);

        $data['home'] = true;
        $data['titulo'] = "Tarefas da Turma - $turma";
        $data['rotulo_botao'] = 'Nova Tarefa';
        $data['form_subject'] = 'nova_tarefa';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('form_tarefa', $data, true);
        $data['lista'] = $this->model->lista_tarefas($turma_id);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para edição das tarefas
     * @param int tarefa_id: o id da tarefa editada
     */
    public function editar($tarefa_id){
        $this->validate_id($tarefa_id);
        $turma_id = $this->model->edita_tarefa($tarefa_id);
        $turma = $this->model->nome_turma($turma_id);
        $data['show_form'] = true;

        $data['titulo'] = "Editar Tarefa da Turma - $turma";
        $data['rotulo_botao'] = 'Nova Tarefa';
        $data['form_subject'] = 'nova_tarefa';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('tarefa/form_tarefa', $data, true);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    public function deletar($tarefa_id){
        $this->validate_id($tarefa_id);
        $data = $this->model->deleta_tarefa($tarefa_id);
        $turma = $this->model->nome_turma($data['turma_id']);

        $data['home'] = true;
        $data['titulo'] = "Remover Tarefa da Turma - $turma";
        $data['turma'] = $this->model->nome_turma($data['turma_id']);
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }

}