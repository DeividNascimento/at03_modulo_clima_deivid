<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FichaClima extends Dao {

    function __construct(){
        parent::__construct('lp2_modulo_climatest');
    }

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('resposta1', 'resposta2', 'resposta3', 'resposta4', 'resposta5');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}