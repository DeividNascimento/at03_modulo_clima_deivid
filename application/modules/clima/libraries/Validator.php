<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function clima_form(){
        
        $this->form_validation->set_rules('resposta1', 'resposta1', 'trim|required|min_length[3]|max_length[10]');
        $this->form_validation->set_rules('resposta2', 'resposta2', 'trim|required|min_length[3]|max_length[10]');
        $this->form_validation->set_rules('resposta3', 'resposta3', 'trim|required|min_length[3]|max_length[10]');
        $this->form_validation->set_rules('resposta4', 'resposta4', 'trim|required|min_length[3]|max_length[10]');
        $this->form_validation->set_rules('resposta4', 'resposta4', 'trim|required|min_length[3]|max_length[10]');
        
        return $this->form_validation->run();
    }

}