-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Jun-2019 às 23:04
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_modulo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clima_questoes`
--

CREATE TABLE `clima_questoes` (
  `id` int(3) NOT NULL,
  `pergunta` varchar(500) NOT NULL,
  `ult_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clima_questoes`
--

INSERT INTO `clima_questoes` (`id`, `pergunta`, `ult_mod`) VALUES
(1, 'Eu recebo feedbacks contínuos sobre o meu desempenho.', '2019-06-20 19:44:18'),
(2, 'Considero minha remuneração adequada às funções que exerço.', '2019-06-20 19:47:11'),
(3, 'Meu time se relaciona de forma harmoniosa e amigável.', '2019-06-20 19:47:18'),
(4, 'Confio na postura de liderança do meu gestor.', '2019-06-20 19:47:24'),
(5, 'Acredito que a empresa oferece serviços/produtos de qualidade aos clientes.', '2019-06-20 19:47:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clima_respostas`
--

CREATE TABLE `clima_respostas` (
  `id` int(3) NOT NULL,
  `resposta1` varchar(10) NOT NULL,
  `resposta2` varchar(10) NOT NULL,
  `resposta3` varchar(10) NOT NULL,
  `resposta4` varchar(10) NOT NULL,
  `resposta5` varchar(10) NOT NULL,
  `ult_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clima_respostas`
--

INSERT INTO `clima_respostas` (`id`, `resposta1`, `resposta2`, `resposta3`, `resposta4`, `resposta5`, `ult_mod`) VALUES
(1, 'Sim', 'Não', 'Às vezes', 'Não', 'Sim', '2019-06-22 21:03:43'),
(2, 'Não', 'Sim', 'Às vezes', 'Não', 'Não', '2019-06-22 21:03:53'),
(3, 'Não', 'Não', 'Sim', 'Sim', 'Sim', '2019-06-22 21:04:01'),
(4, 'Sim', 'Sim', 'Às vezes', 'Não', 'Não', '2019-06-22 21:04:26'),
(5, 'Sim', 'Sim', 'Sim', 'Sim', 'Sim', '2019-06-22 21:04:34'),
(6, 'Não', 'Não', 'Não', 'Não', 'Não', '2019-06-22 21:04:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lp2_modulo_climatest`
--

CREATE TABLE `lp2_modulo_climatest` (
  `id` int(3) NOT NULL,
  `resposta1` varchar(10) NOT NULL,
  `resposta2` varchar(10) NOT NULL,
  `resposta3` varchar(10) NOT NULL,
  `resposta4` varchar(10) NOT NULL,
  `resposta5` varchar(10) NOT NULL,
  `ult_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefa_turma`
--

CREATE TABLE `tarefa_turma` (
  `id` int(11) NOT NULL,
  `titulo` varchar(128) NOT NULL,
  `prazo` date NOT NULL,
  `descricao` varchar(512) NOT NULL,
  `turma_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tarefa_turma`
--

INSERT INTO `tarefa_turma` (`id`, `titulo`, `prazo`, `descricao`, `turma_id`, `deleted`, `last_modified`) VALUES
(1, 'Atividade 01', '2019-06-05', 'Uma tarefa bem difícil', 5, 0, '2019-06-21 04:15:22'),
(3, 'Tarefa muito atrasada', '2019-06-03', 'Caso de reprovar o aluno', 7, 0, '2019-06-21 04:15:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clima_questoes`
--
ALTER TABLE `clima_questoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clima_respostas`
--
ALTER TABLE `clima_respostas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lp2_modulo_climatest`
--
ALTER TABLE `lp2_modulo_climatest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tarefa_turma`
--
ALTER TABLE `tarefa_turma`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clima_questoes`
--
ALTER TABLE `clima_questoes`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clima_respostas`
--
ALTER TABLE `clima_respostas`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lp2_modulo_climatest`
--
ALTER TABLE `lp2_modulo_climatest`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tarefa_turma`
--
ALTER TABLE `tarefa_turma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
